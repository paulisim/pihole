# Pihole with local zone and dnscrypt-proxy

To use on other platform than aarch64 edit `roles/dnscrypt/tasks/main.yaml` line 12. Going to fix that some day, maybe ..

1. install Pihole
2. set Pihole _Upstream DNS Servers_ to  _127.0.0.1#5533_ and _Interface listening behavior_ to _Listen only on interface_
3. edit inventory
4. run playbook